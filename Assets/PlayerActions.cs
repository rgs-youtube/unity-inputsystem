// Copyright (C) 2023 - Retrocoder Games <j.karlsson@retrocoder.se>

using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerActions : MonoBehaviour {
  public InputActionAsset actionAsset;

  private Vector2 moveDirection = Vector2.zero;

  private void Awake() {
    var actionMap = actionAsset.FindActionMap("GamePlay");
    actionMap.FindAction("Move").performed += OnMove;
    actionMap.FindAction("Move").canceled += OnMove;
  }

  private void OnDestroy() {
    var actionMap = actionAsset.FindActionMap("GamePlay");
    actionMap.FindAction("Move").performed -= OnMove;
    actionMap.FindAction("Move").canceled -= OnMove;
  }

  private void OnEnable() {
    actionAsset.FindActionMap("GamePlay").Enable();
  }

  private void OnDisable() {
    actionAsset.FindActionMap("GamePlay").Disable();
  }

  private void OnMove(InputAction.CallbackContext ctx) {
    moveDirection = ctx.ReadValue<Vector2>();
  }

  // Update is called once per frame
  void Update() {
    transform.position += new Vector3(moveDirection.x * 0.01f, moveDirection.y * 0.01f, 0);
  }
}

// --- License ---
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
// or write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

