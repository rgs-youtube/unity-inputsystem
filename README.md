# Unity InputSystem
A simple example project in Unity showing an easy way to use Unitys new InputSystem

# Youtube channel
This is exmple code from my youtube video using the InputSystem:
Watch it here: https://youtu.be/3Sc85gqDGEU

See also my youtube channel
https://www.youtube.com/@retrocoder_games

# Licence
All code in this project is licenced under GPLv2.
See https://www.gnu.org/licenses/

All art and graphics, as well as music and sounds is licenced under Creative Common Licence CC-BY-SA 4.0
See https://creativecommons.org/licenses/by-sa/4.0/

